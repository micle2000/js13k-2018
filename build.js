var u = require("uglify-js");
u.css = require("uglifycss");
var fs = require("fs");
var child_process = require("child_process");

var files = [];
var cssFiles = [];
var imageFiles = [];
var images = {};

var input = "";
fs.readdirSync("shared").forEach(file => {
	files.push("shared/" + file);
	input += fs.readFileSync("shared/"+file);
});
fs.readdirSync("game").forEach(file => {
	files.push("game/" + file);
	input += fs.readFileSync("game/"+file);
});
fs.readdirSync("css").forEach(file => {
	cssFiles.push("css/" + file);
});
fs.readdirSync("images").forEach(file => {
	imageFiles.push(file);
	var data = fs.readFileSync("images/"+file);
	images[file] = Buffer.from(data).toString("base64");
});

var result = u.minify(input, {toplevel: true});
if (result.error) {
	console.error(result.error);
	return;
}
var code = result.code;

var css = u.css.processFiles(cssFiles);

var template = `
<html>
<head>
<title>{title}</title>
{style}
</head>
<body>
{images}
{code}
</body>
</html>
`;

template = template.replace(/\s/g,'');

var output = template
.replace("{title}", "js13k!")
.replace("{style}", `<style>${css}</style>`)
.replace("{code}", `<script>${code}</script>`)
.replace("{images}", Object.keys(images).map(i => `<img id="${i}" src="data:image/png;base64,${images[i]}" />`).join(''))
;

var debugOutput = template
.replace("{title}", "Debug js13k")
.replace("{code}", files.map(f => `<script src="../${f}"></script>`).join(""))
.replace("{style}", cssFiles.map(f => `<link rel="stylesheet" type="text/css" href="../${f}">`).join(""))
.replace("{images}", imageFiles.map(i => `<img id="${i}" src="../images/${i}" />`).join(''))
;

if (!fs.existsSync("dist")) {
	fs.mkdirSync("dist");
}
if (!fs.existsSync("debug")) {
	fs.mkdirSync("debug");
}

fs.writeFileSync("dist/js13k.html", output);
fs.writeFileSync("debug/js13k.html", debugOutput);

function sh() {
	var result = child_process.execSync(...arguments);
	console.log(result.toString());
}

sh("zip -R js13k.zip dist/*");

var stats = fs.statSync("js13k.zip");
console.log("Size:");
console.log(stats.size + " / " + (13*1024));
console.log((stats.size / (13*1024) * 100).toFixed(1) + "%");
console.log("");

