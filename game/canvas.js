var canvas = document.createElement("canvas");
document.body.appendChild(canvas);
var gl = canvas.getContext("2d");

canvas.width = 160;
canvas.height = 144;

canvas.style.width = canvas.width;
var bodyWidth = document.body.getBoundingClientRect().width;
var bodyHeight = document.body.getBoundingClientRect().height;
var canvasWidth = canvas.width;
var canvasHeight = canvas.height;
while (canvasWidth*2 < bodyWidth && canvasHeight*2 < bodyHeight) {
	canvasWidth *= 2;
	canvasHeight *= 2;
}
canvas.style.width = canvasWidth;


